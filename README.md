### This is repository for my thesis:

## Creation of model learning playing tetris using reinforcement learning

This repository is mostly based on those two:

https://github.com/yenchenlin/DeepLearningFlappyBird

https://github.com/asrivat1/DeepLearningVideoGames

Main difference is that there we are solving Tetris game, so all of things dependending on game are different (whole game wrapper, neural network output interpretation etc.). Also, there was a change in neural network structure, so now it works faster and is adjusted for specific game screen data. Apart from that, there's many differences like slightly changes in the results logging etc., there was too much of them to note all.

Game used to run the learning algorithm is MaTris [1]. Ofcourse slightly modificated - works in the background, all bricks are white and their amount is decreased, steering method was changed too. Also, there was quite a lot changes with eg. speed of falling brick, so I can tweak all things together to make game usable by neural network in the best efficiency. 


[1] http://www.pygame.org/project-MaTris-2827-.html