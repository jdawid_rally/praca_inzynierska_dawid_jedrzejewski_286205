import numpy as np
import cv2
import os
import ctypes
import sys
import datetime
import pygame
from PIL import Image
from .MaTris.constants import *
from .MaTris.game import Game

class GameState(object):

	def __init__(self):
		self.game = Game()
		self.game.initialize()
		self.score = 0

	'''
		Literally reward function.
		Triggers the frame to be created, then counts the reward to return
		Also gathers screen info and bool, if last frame was ending an episode
	'''
	def frame_step(self, input_actions):
		game_state = self.game.play(self.parse_action(input_actions))
		if not game_state:
			self.game.initialize()

		old_points = self.score
		new_points = self.grabPoints()
		reward = (new_points - old_points)
		if reward < 0:
			terminal = True
			reward = -100
		else:
			terminal = False
			reward = reward
		if reward == 0 and not game_state:
			reward = -100
		self.score = new_points
		
		if reward == 0:
			reward = -1*self.countHeightDiff()

		image_data = self.grabImage()
		return image_data, reward, terminal

	'''
		Just prettyfier for taking points from game instance
	'''
	def grabPoints(self):
		return self.game.matris.score

	'''
		Retrieve, and preprocess image.
		Uncomment lines in the middle to have image displayed.
	'''
	def grabImage(self):
		image_data = pygame.surfarray.array3d(pygame.display.get_surface())
		image_data = np.swapaxes(image_data, 0, 1)
		c_img = image_data
		c_img = c_img[30:630, 30:330].copy()
		#cv2.imshow('screen', c_img)	
		#cv2.waitKey(1)
		c_img = cv2.resize(c_img, (10,22), interpolation = cv2.INTER_AREA)
		c_img = cv2.cvtColor(c_img, cv2.COLOR_BGR2GRAY)
		ret, c_img = cv2.threshold(c_img, 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)
		c_img = np.reshape(c_img, (22, 10, 1))	
		return c_img

	'''
		Counts difference between lowest hole and highest fixed block
	'''
	def countHeightDiff(self):
		table = self.game.matris.matrix
		lowest_hole_index = 20
		highest_block_index = 0
		has_none = False
		has_value = False
		for row_index in range(0, 21):
			for col_index in range(0, 9):
				if table[(row_index,col_index)] == None:
					has_none = True
				else:
					has_value = True
			if highest_block_index == 0 and has_value == True:
				highest_block_index = row_index
			if has_none:
				lowest_hole_index = row_index
			has_none = False
			has_value = False

		if lowest_hole_index == 20 and highest_block_index == 0: #assume that table is empty or only 1st row filled
			return 0
		else:
			return lowest_hole_index - highest_block_index #as indexes go from up to down
	
	'''
		Translate action given from network to format which is processible by game
	'''
	def parse_action(self, input_actions):
		if input_actions[0] == 1:
			return 'nothing'
		if input_actions[1] == 1:
			return 'up'
		if input_actions[2] == 1:
			return 'left'
		if input_actions[3] == 1:
			return 'right'
			