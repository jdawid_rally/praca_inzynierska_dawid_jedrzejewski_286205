from constants import *
import pygame
from pygame import Rect, Surface
import random
import os
import kezmenu
from menu import Menu

from tetrominoes import list_of_tetrominoes
from tetrominoes import rotate

from scores import load_score, write_score


if __name__ == '__main__':
    pygame.init()

    screen = pygame.display.set_mode((WIDTH, HEIGHT))
    pygame.display.set_caption("MaTris")
    Menu().main(screen)